const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const multer = require('multer');

const app = express();
const upload = multer(); // for parsing multipart/form-data
const port = 3000;

app.use(express.static('dist'));
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../../dist', 'index.html'));
});

app.post('/submitted', upload.array(), (req, res) => {
  console.log('Form data: ', req.body);
  res.status(200).end();
});

app.listen(port, () => {
  console.log('Server listening on port 3000');
});
