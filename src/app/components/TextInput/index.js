import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './text-input.scss';

class TextInput extends Component {
  render() {
    const {
      type,
      id,
      value,
      placeholder,
      onInputChange,
    } = this.props;

    return (
      <input
        type={type}
        className="form__input"
        id={id}
        value={value}
        placeholder={placeholder}
        onChange={onInputChange}
      />
    );
  }
}

TextInput.propTypes = {
  type: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onInputChange: PropTypes.func.isRequired,
};

export default TextInput;
