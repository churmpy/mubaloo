import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextInput from '../TextInput';
import Button from '../Button';

import './form.scss';

class Form extends Component {
  render() {
    const {
      inputs,
      buttonProps,
      onInputChange,
    } = this.props;

    return (
      <div>
        <form className="form">
          {inputs.map((input, key) => (
            <TextInput
              type={input.type}
              id={input.id}
              value={input.value}
              placeholder={input.placeholder}
              key={key}
              onInputChange={onInputChange}
            />
          ))}
        </form>
        <Button
          type={buttonProps.type}
          text={buttonProps.text}
          color={buttonProps.color}
          backgroundColor={buttonProps.backgroundColor}
          borderColor={buttonProps.borderColor}
          onClick={buttonProps.onClick}
        />
      </div>
    );
  }
}

Form.propTypes = {
  inputs: PropTypes.instanceOf(Array).isRequired,
  buttonProps: PropTypes.instanceOf(Object).isRequired,
  onInputChange: PropTypes.func.isRequired,
};

export default Form;
