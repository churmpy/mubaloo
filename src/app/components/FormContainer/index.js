import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Form from '../Form';
import StepTracker from '../StepTracker';

import './form-container.scss';

class FormContainer extends Component {
  render() {
    const {
      inputs,
      buttonProps,
      onInputChange,
      step,
    } = this.props;

    return (
      <div className="form-container">
        <header className="form-container__header">
          <h1 className="form-container__header__title">Fill in your details</h1>
        </header>
        <div className="form-container__step-tracker__wrapper">
          <StepTracker step={step} />
        </div>
        <Form
          inputs={inputs}
          buttonProps={buttonProps}
          onInputChange={onInputChange}
        />
      </div>
    );
  }
}

FormContainer.propTypes = {
  inputs: PropTypes.instanceOf(Array).isRequired,
  buttonProps: PropTypes.instanceOf(Object).isRequired,
  onInputChange: PropTypes.func.isRequired,
  step: PropTypes.string.isRequired,
};

export default FormContainer;
