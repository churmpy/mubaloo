import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './button.scss';

class Button extends Component {
  render() {
    const {
      type,
      text,
      color,
      backgroundColor,
      borderColor,
      onClick,
    } = this.props;

    const buttonStyle = {
      color,
      backgroundColor,
      borderColor,
    };

    return (
      <button
        type={type}
        className="button"
        onClick={onClick}
        style={buttonStyle}
      >
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  type: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  borderColor: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Button;
