import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './step-tracker.scss';

class StepTracker extends Component {
  render() {
    const { step } = this.props;

    if (step === 'stepOne') {
      return <div className="step-tracker">Step 1 of 2</div>;
    }
    if (step === 'stepTwo') {
      return <div className="step-tracker">Step 2 of 2</div>;
    }
  }
}

StepTracker.propTypes = {
  step: PropTypes.string.isRequired,
};

export default StepTracker;
