import React, { Component } from 'react';

import './confirmation.scss';
import tick from './img/green_tick.png';

class Confirmation extends Component {
  render() {
    return (
      <div className="confirmation">
        <img src={tick} alt="Confirmation tick" className="confirmation__image" />
        <p className="confirmation__text">Thanks for your submission</p>
      </div>
    );
  }
}

export default Confirmation;
