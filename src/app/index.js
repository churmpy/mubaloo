import React, { Component } from 'react';

import FormContainer from './components/FormContainer';
import Confirmation from './components/Confirmation';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 'stepOne',
      formData: {
        title: '',
        name: '',
        dateOfBirth: '',
        location: '',
        feedback: '',
      },
    };

    this.onContinue = this.onContinue.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(event) {
    const {
      id,
      value,
    } = event.target;

    this.setState(prevState => (
      {
        formData: {
          ...prevState.formData,
          [id]: value,
        },
      }
    ));
  }

  onContinue() {
    const {
      formData: {
        title,
        name,
        dateOfBirth,
      },
    } = this.state;

    if (title.length !== 0 && name.length !== 0 && dateOfBirth.length !== 0) {
      this.setState({
        step: 'stepTwo',
      });
    }
  }

  onSubmit() {
    const {
      formData: {
        location,
        feedback,
      },
    } = this.state;

    if (location.length !== 0 && feedback.length !== 0) {
      const dateTime = new Date();

      const payload = {
        ...this.state.formData,
        dateTime,
      };

      const that = this;

      fetch('/submitted', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload),
      })
        .then((res) => {
          if (res.status === 200) {
            that.setState({
              step: 'submitted',
            });
          }
        })
        .catch((res) => { console.log(res); });
    }
  }

  render() {
    const {
      step,
      formData,
    } = this.state;
    const stepOneInputs = [
      {
        type: 'text',
        id: 'title',
        value: formData.title,
        placeholder: 'Title',
      },
      {
        type: 'text',
        id: 'name',
        value: formData.name,
        placeholder: 'Name',
      },
      {
        type: 'text',
        id: 'dateOfBirth',
        value: formData.dateOfBirth,
        placeholder: 'Date of Birth',
      },
    ];
    const stepOneButton = {
      type: 'button',
      text: 'Continue',
      color: '#fff',
      backgroundColor: '#337ab7',
      borderColor: '#2e6da4',
      onClick: this.onContinue,
    };
    const stepTwoInputs = [
      {
        type: 'text',
        id: 'location',
        value: formData.location,
        placeholder: 'Current location',
      },
      {
        type: 'text',
        id: 'feedback',
        value: formData.feedback,
        placeholder: 'Feedback',
      },
    ];
    const stepTwoButton = {
      type: 'submit',
      text: 'Submit',
      color: '#fff',
      backgroundColor: '#5cb85c',
      borderColor: '#4cae4c',
      onClick: this.onSubmit,
    };

    if (step === 'stepOne') {
      return (
        <FormContainer
          inputs={stepOneInputs}
          buttonProps={stepOneButton}
          onInputChange={this.onInputChange}
          step={step}
        />
      );
    } if (step === 'stepTwo') {
      return (
        <FormContainer
          inputs={stepTwoInputs}
          buttonProps={stepTwoButton}
          onInputChange={this.onInputChange}
          step={step}
        />
      );
    } if (step === 'submitted') {
      return <Confirmation />;
    }
  }
}

export default App;
