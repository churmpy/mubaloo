# Mubaloo tech test

A Node.js application that posts form data to the server. The server then logs the data to the console.

## Instructions

1. Clone this repo
2. Run `npm install`
3. Run `npm start`, **localhost:3000** will open up in your default browser

## Tools used

For the project I used webpack as my build tool, react for the front end, sass for styling, express for the API, and eslint (extended with the airbnb config) as my linter.

## Improvements for the future

If I were able to take the test again I would look to improve/add the following:

1. Add state to the Form component so the step inputs and button data did not have to be passed down all the way from the App parent. The formData could then be passed up to the parent to be sent to the server.
2. Add unit testing. Previously I have not had to set up/configure Jest and Enzyme from scratch so this would have taken me more time than I have to complete. I'll be looking to learn this set up process in the near future.
3. Making the feedback input size larger to encourage and accomodate a more detailed answer.